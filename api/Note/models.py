from django.db import models

# Create your models here.

class Note(models.Model):
    title = models.CharField(max_length = 60, blank=True)
    content = models.CharField(max_length = 260, blank=False)
    created_on = models.DateTimeField(auto_now_add=True, verbose_name='Note created on')
    last_modified = models.DateTimeField(auto_now=True, verbose_name='Note last updated on')

    def __str__(self):
        return f'{self.title}'