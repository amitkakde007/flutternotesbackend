from django.test import TestCase
from Note.models import Note

class NoteModelTests(TestCase):

    def test_create_note(self):
        """Test creating a note is successfull"""

        title='Test note-1'
        content='Creating this to test Note model'
        note = Note.objects.create(
            title = title,
            content=content
        )

        self.assertEqual(note.title,title)
        self.assertEqual(note.content,content)
    

    def test_update_note(self):
        """Test updating the note"""

        title='Test note-1'
        content='Creating this to test Note model'
        updated_content='Updating existing note'
        note = Note.objects.create(
            title = title,
            content=content
        )

        note.content = updated_content
        note.save()
        self.assertEqual(note.content,updated_content)
    
    def test_delete_note(self):
        """Test deleting the note"""

        title='Test note-1'
        content='Creating this to test Note model'
        
        note = Note.objects.create(
            title = title,
            content=content
        )

        self.assertEqual(note.title,title)
        self.assertEqual(note.content,content)
        
        with self.assertRaises(AssertionError) as e:
            delete_note = Note.objects.get(pk=note.id)
            delete_note.delete()
            self.assertIn(delete_note,Note.objects.all())
        self.assertEqual(str(e.exception),'<Note: Test note-1> not found in <QuerySet []>')

    