from django.test import TestCase, Client
from django.urls import reverse


from rest_framework import status

from Note.views import NoteViewSet
from Note.models import Note
from Note.serializers import NoteSerializer

class TestCreateNoteViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = 'note:note-viewset'
        self.valid_note = {
            'title':'Created from testcase-1',
            'content':'Created testcase-1 for detail view of note routers'
        }
        self.invalid_note = {
            'title':'Created from testcase-1',
            'content':''
        }

    def test_valid_data_note_view(self):

        response = self.client.post(reverse(f'{self.url}-list'),
            data=self.valid_note,
            content='application/json'
        )
        self.assertEqual(response.status_code,status.HTTP_201_CREATED)
    
    def test_invalid_data_note_view(self):

        response = self.client.post(reverse(f'{self.url}-list'),
            data=self.invalid_note,
            content='application/json'
        )
        self.assertEqual(response.status_code,status.HTTP_400_BAD_REQUEST)


class TestRetrieveNoteViews(TestCase):

    def setUp(self):
        self.url = 'note:note-viewset'
        self.client = Client()

        self.note1 = Note.objects.create(
            title='Created from testcase-1',
            content='Created testcase-1 for detail view of note routers'
            )
        self.note2 = Note.objects.create(
            title='Created from testcase-2',
            content='Created testcase-2 for detail view of note routers'
            )

    def test_note_get_list_view(self):
        
        # API response
        response  = self.client.get(reverse(f'{self.url}-list'))
        # Db data
        notes = NoteSerializer(Note.objects.all(), many=True)
        #notes.is_valid(raise_exception=True)
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data,notes.data)
    
    def test_note_post_detail_view(self):

        #API response
        response = self.client.get(reverse(f'{self.url}-detail',kwargs={'pk':self.note2.pk}))
        # DB data
        data = Note.objects.get(pk=2)
        notes = NoteSerializer(data)
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data,notes.data)


class TestUpdateNoteViews(TestCase):

    def setUp(self):
        self.url = 'note:note-viewset'
        self.client = Client()
        self.note1 = Note.objects.create(
            title='Created from testcase-1',
            content='Created testcase-1 for detail view of note routers'
            )
        self.note2 = Note.objects.create(
            title='Created from testcase-2',
            content='Created testcase-2 for detail view of note routers'
            )
        self.valid_update_note2={'content':'Testing update fnction with testcase class'}
        self.invalid_update_note2={'content':''}

    def test_note_update_view(self):

        response = self.client.put(reverse(f'{self.url}-detail', 
            kwargs={'pk':self.note2.pk}), 
            data=self.valid_update_note2, 
            content_type='application/json')
        self.assertEqual(response.status_code,status.HTTP_200_OK)

    def test_no_data_passed(self):

        response = self.client.put(reverse(f'{self.url}-detail', 
            kwargs={'pk':self.note2.pk}),
            data=self.invalid_update_note2,
            content='application/json')
        self.assertEqual(response.status_code,status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
    
    def test_invalid_pk_passed(self):

        response = self.client.put(reverse(f'{self.url}-detail', kwargs={'pk':30}), data=self.valid_update_note2, content='application/json')
        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)


class TestDeleteNoteViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = 'note:note-viewset'
        self.note1 = Note.objects.create(
            title='Created from testcase-1',
            content='Created testcase-1 for detail view of note routers'
            )
        self.note2 = Note.objects.create(
            title='Created from testcase-2',
            content='Created testcase-2 for detail view of note routers'
        )
    
    def test_single_object(self):

        response = self.client.delete(reverse(f'{self.url}-detail', kwargs={'pk':self.note1.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    
    def test_invalid_id(self):

        response = self.client.delete(reverse(f'{self.url}-detail', kwargs={'pk':23}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)