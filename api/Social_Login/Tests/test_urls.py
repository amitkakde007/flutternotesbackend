from django.test import SimpleTestCase
from django.urls import reverse, resolve

from rest_framework_simplejwt.views import TokenObtainSlidingView, TokenRefreshSlidingView

from Social_Login.views import GoogleAuthView


class TestUrls(SimpleTestCase):

    def test_google_auth_url_resovlve(self):
        url = reverse('google-login')
        self.assertEquals(resolve(url).func.view_class, GoogleAuthView)

    
    def test_token_obtain_url_resolve(self):
        url = reverse('token-obtain')
        self.assertEqual(resolve(url).func.view_class, TokenObtainSlidingView)
    
    def test_refresh_token_url_resolve(self):
        url = reverse('token-refresh')
        self.assertEqual(resolve(url).func.view_class, TokenRefreshSlidingView)