from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView

from .serializer import GoogleAuthSerializer

class GoogleAuthView(GenericAPIView):

    serializer_class = GoogleAuthSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            data = ((serializer.validated_data)['auth_token'])
            return Response(data, status=status.HTTP_200_OK)
