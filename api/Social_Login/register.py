from environs import Env

from django.contrib.auth import authenticate

from rest_framework.exceptions import AuthenticationFailed

from .models import CustomerUser

env = Env()
env.read_env('../.env')


def register_social_user(provider, user_id, email, name):
    filtered_user_by_email = User.objects.filter(email=email)

    if filtered_user_by_email.exists():
        registered_user = authenticate(
            email=email,
            password=env('SOCIAL_SECRET')
            )
        return({
            'email': registered_user.email,
            'token': registered_user.tokens()
            })
    else:
        user = {
            'email':email,
            'password': env('SOCIAL_SECRET')
            }
        user = CustomerUser.objects.create_user(**user)
        user.is_verified = True
        user.auth_provider = provider()
        user.save()

        new_user = authenticate(
            email=email, password=env('SOCIAL_SECRET')
        )
        return({
            'email': registered_user.email,
            'token': registered_user.tokens()
            })
