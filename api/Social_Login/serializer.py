from environs import Env

from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed

from . import google

env = Env()
env.read_env('../.env')

class GoogleAuthSerializer(serializers.Serializer):
    auth_token = serializers.CharField()

    def validate_auth_token(self):
        user_data = google.Google.validate(self.auth_token)
        try:
            user_data = ['sub']
        except:
            raise serializer.ValidationError(
                'The token is expired or invalid!'
            )
        
        if user_data['aud'] != env('GOOGLE_CLIENT_ID'):
            raise AuthenticationFailed("Sorry, couldn't authenticate you. NONE SHALL PASS!!!")
        
        user_id = user_data['sub']
        email = user_data['email']
        name = user_data['name']
        provider = 'google'

        return register_social_user(
            provider=provider, user_id=user_id,email=email,name=name
        )