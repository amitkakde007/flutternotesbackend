from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, AbstractUser
from django.db import models

from rest_framework_simplejwt.tokens import RefreshToken
# Create your models here.

class UserManager(BaseUserManager):

    def create_user(self, email, password=None):

        if not email:
            raise ValueError('Please provide email-id')
        
        user = self.model(
            email = self.normalize_email(email)
            )

        user.set_password(password)
        user.save(using=self.db)
        return user()

    def create_superuser(self, email, password=None):
        if not email:
            raise ValueError('Please provide email-id')
        
        if not password:
            raise ValueError('Please provide password')

        user = self.model(
            email = self.normalize_email(email)
            )
        user.is_admin = True

        user.set_password(password)
        user.save(using=self.db)
        return user


class CustomUser(AbstractBaseUser):

    firstname = models.CharField(verbose_name='First Name', max_length=60)
    lastname = models.CharField(verbose_name='Last Name', max_length=60)
    email = models.EmailField(verbose_name='Email Address', max_length=255, unique=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_staff = True
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELD = ['email']

    def __str__(self):
        return f'{self.firstname} {self.lastname}'

    def get_email(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True


class CustomerUser(CustomUser):

    is_customer = True
    is_verfied = models.BooleanField(default=False)


    def __str__(self):
        return f'{self.firstname} {self.lastname}'
    
    def get_email(self):
        return self.get_email()

    def token(self):
        refresh = RefreshToken.for_user(self)
        return{
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }

class AdminUser(CustomUser):

    is_admin = models.BooleanField(default=True)


    def __str__(self):
        return f'{self.firstname} {self.lastname}'
    
    def get_email(self):
        return self.get_email()
    
    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin